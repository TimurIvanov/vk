function initBootstrapForms() {
    $("form.bootstrap-form").find("input,textarea").addClass("form-control");
    $("form.bootstrap-form").find("input[type='submit']").removeClass("form-control");
}

$(document).ready(function(){
    initBootstrapForms();
    let input = document.getElementById("post-input");
    let block = document.getElementById("post-footer");
    let margin = document.getElementById("post-margin");

    input.onfocus = function() {
        block.classList.remove('hidden');
        margin.classList.add('post-margin');
    };

});