from vk_posts.models import User


def register_user(username: str, email: str, first_name: str, last_name: str, password: str) -> User:
    user = User(username=username, email=email, first_name=first_name, last_name=last_name)
    user.set_password(password)
    user.save()
    return user
