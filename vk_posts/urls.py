from django.urls import path, re_path
from django.conf import settings
from django.views.static import serve
from vk_posts.views import HomeView, LogoutView, LoginView, RegisterView, ProfileView, EditProfileView


urlpatterns = [
    path('', HomeView.as_view(), name="home"),
    path('accounts/login/', LoginView.as_view(), name="login"),
    path('accounts/logout/', LogoutView.as_view(), name="logout"),
    path('accounts/register/', RegisterView.as_view(), name="register"),
    path('accounts/profile/', ProfileView.as_view(), name="profile"),
    path('accounts/profile/edit/', EditProfileView.as_view(), name="edit_profile"),
    re_path(r"^static/(?P<path>.*)$", serve, {"document_root": settings.STATIC_ROOT}),
]
