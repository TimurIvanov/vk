from random import randint

from django import template


register = template.Library()


@register.simple_tag
def random_int(a, b):
    return randint(a, b)
