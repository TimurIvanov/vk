```python3 -m venv .venv``` - создать виртуальное окружение

```.\venv\Scripts\activate.bat``` - войти в виртуальное окружение

```pip install -r requirements.txt``` - установить зависимости

```Подключить PostgreSQL к проекту```

```mkdir media``` - создать папку media в корне проекта

```python manage.py migrate``` - применить миграции к базе данных

```python manage.py runserver``` - запуск сервера для разработки
