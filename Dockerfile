# pull official base image
FROM python:3.9.6

# set work directory
WORKDIR /app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
RUN pip install --upgrade pip
COPY requirements.txt .
RUN pip3 install --no-cache-dir -r requirements.txt

# copy project
COPY . .

ENV PORT 8000

CMD python manage.py migrate && \
    python manage.py collectstatic && \
    gunicorn --bind 0.0.0.0:$PORT django_project.wsgi
